import React from "react";
import Trace from "./Trace/Trace";
import createApolloClient from "./config/apollo";
import { ApolloProvider } from "@apollo/client";

function App() {
  const client = createApolloClient();

  return (
    <ApolloProvider client={client}>
      <Trace />
    </ApolloProvider>
  );
}

export default App;
