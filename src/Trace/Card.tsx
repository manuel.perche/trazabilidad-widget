import { PUBLIC_SERVER_ASSETS } from "../utils/constants";
import Verification from "./Verification";

function Card(props: any) {
  const {
    id,
    title, //Va en el header
    subtitle, //Va en el header grisado
    icon, // iconito a la izquierda de la tarjeta.
    heading, //Heading del primer item de body
    location, //bajada del primer item del body, va grisado.
    outputIcon, //imagen del producto. H: , W:
    by, //Empresa que realiza el proceso.
    logoBy, // Logo de la Empresa que realiza el proceso. Opcional
    logo_right,
    inputs, //Que se usó para llegar al producto final.
    evidence, // Array con los objetos de evidencia.
    batch,
  } = props;

  return (
    <div className="card border-light shadow my-3">
      <div className="card-header bg-transparent border-ligh">
        <p className="mb-0">
          {title} <small className="text-muted">{subtitle}</small>
        </p>
        {batch && batch.lote_semilla ? (
          <p className="mb-0">
            <small>Lote Semilla: {batch.lote_semilla}</small>
          </p>
        ) : null}
        {batch && batch.calibre ? (
          <p className="mb-0">
            <small>Calibre: {batch.calibre}</small>
          </p>
        ) : null}
        {batch && batch.variedad ? (
          <p className="mb-0">
            <small>Variedad: {batch.variedad}</small>
          </p>
        ) : null}
      </div>
      <div className="card-body">
        <div className="d-flex align-items-center justify-content-between p-1">
          {outputIcon && <img src={`${PUBLIC_SERVER_ASSETS}/${outputIcon}`} className="m-2" alt="" width="60" height="60" />}
          <div className="flex-fill">
            <p className="card-title mb-0">{heading}</p>
            <p className="card-subtitle mb-2 text-muted">
              <small>{location}</small>
            </p>
          </div>
        </div>

        {inputs && (
          <div className="d-flex align-items-center justify-content-between p-1">
            <div className="flex-fill">
              <p className="card-title text-muted mb-0">
                <small>Hecho con</small>
              </p>
              <p className="card-subtitle fw-light mb-2">{inputs}</p>
            </div>
          </div>
        )}
        {by && (
          <div className="d-flex align-items-center justify-content-between p-1">
            {logoBy && <img src={`${PUBLIC_SERVER_ASSETS}/${logoBy}`} className="m-2" alt="" width="32" height="32" />}
            <div className="flex-fill">
              <p className="card-subtitle text-muted mb-0">
                <small>Hecho por</small>
              </p>
              <p className="card-title mb-2">{by}</p>
            </div>

            {logo_right && (
              <img src={`../assets/${PUBLIC_SERVER_ASSETS}/sections/trace/${logo_right}`} className="m-2" alt="" width="32" height="32" />
            )}
          </div>
        )}
        {evidence.length ? <Verification id={id} data={evidence} /> : null}
      </div>
    </div>
  );
}

export default Card;
