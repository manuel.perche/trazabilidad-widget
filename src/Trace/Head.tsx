import { PUBLIC_SERVER_ASSETS } from "../utils/constants";

function Head(props: any) {
    const {icon, line1, line2, line3} = props

  return (
    <div>
      {icon &&
        <img
          src={`./assets/${PUBLIC_SERVER_ASSETS}${icon}`}
          className="sectionIcon"
          alt="..."
        />
      }

        <div className="container text-center mt-0 section-title">
          <p className="lead mb-0">{line1}</p>
          <h2 className="mb-0">{line2}</h2>
          <p className="lead">{line3}</p>
        </div>
      </div>
  );
}

export default Head;
