import { GET_STAGES } from "../graphql/getStages";
import Head from "./Head";
import { useQuery } from "@apollo/client";
import Timeline from "./Timeline";
import Card from "./Card";
import { GET_QR_BATCHES } from "../graphql/getQrBatches";

function Trace(props: any) {
  const { data: { product_by_id: { stages = {} } = {} } = {} } = useQuery(GET_STAGES, {
    variables: {
      id: "9b3b048c-03b6-42ce-a48a-9789eb4c91af",
    },
  });

  const { data: { qr_by_id: { batches = {} } = {} } = {} } = useQuery(GET_QR_BATCHES, {
    variables: {
      id: "a038a37c-1d52-4c01-870a-b4fe2ea86729",
    },
  });

  return (
    <div className={"trace container section"}>
      <Timeline>
        {stages.length > 0 &&
          stages.map((card: any, index: any) => {
            const batch = batches.length > 0 ? batches.find((batch: any) => batch.stage_id.id === card.id) : {};

            const batchEvidences = batch?.evidences?.map((ev: any) => {
              return {
                evidence_id: ev,
              };
            });

            const evidences = batch && batch?.evidences?.length > 0 ? [...card.evidences, ...batchEvidences] : card.evidences;

            const heading = batch && batch.quantity ? `${batch.quantity} ${batch.unit} de ${card.output}` : card.output;
            return (
              <Card
                id={card.id}
                key={index}
                type={card.type}
                title={card.title}
                subtitle={batch ? batch.batch_number : card.subtitle}
                icon={card.icon}
                heading={heading}
                location={card.location}
                outputIcon={card.output_icon?.id}
                by={card.by}
                logoBy={card.logo_by?.id}
                logo_right={card.logo_right}
                inputs={card.inputs}
                evidence={evidences}
                batch={batch}
              />
            );
          })}
      </Timeline>
    </div>
  );
}

export default Trace;
