import { gql } from "@apollo/client";

export const GET_QR_BATCHES = gql`
  query qr_by_id($id: ID!) {
    qr_by_id(id: $id) {
      batches {
        id
        batch_number
        name
        quantity
        unit
        stage_id {
          id
        }
        evidences {
          id
          title
          subtitle
          icon {
            id
            title
          }
          logo {
            id
            title
          }
          description
          evidence {
            id
            title
          }
          ots {
            id
            title
          }
          link
        }
      }
    }
  }
`;
