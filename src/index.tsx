import React from "react";
import ReactDOM from "react-dom";
import App from "./App";
import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap/dist/js/bootstrap.min.js'

const widgetDiv = document.querySelector("#trazabilidad");

ReactDOM.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
  widgetDiv
);
